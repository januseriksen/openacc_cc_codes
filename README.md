GPU-accelerated RI-MP2 and CCSD(T) codes using OpenACC compiler directives.

The codes were prepared to accompany the work described in:

 * Eriksen, J. J.: "Efficient and portable acceleration of quantum chemical many-body methods in mixed floating point precision using OpenACC compiler directives", Molecular Physics (MQM2016 special issue), DOI: http://dx.doi.org/10.1080/00268976.2016.1271155 (2016).
 * ALTERNATIVELY: Eriksen, J. J.: arXiv:1609.08094 (2016)

******

Author: 

Dr. Janus Juul Eriksen

Time: 

July and August, 2016

Places: 

Aarhus University, Denmark, 

San Jose, CA, USA,

Johannes Gutenberg-University Mainz, Germany

******

A Makefile has been prepared for all of the code versions.
These assume that a PGF90 (PGI) compiler is available
(versions 15.10 and 16.4 have been tested).

The codes only work with the PGI compiler, as these make use
of CUDA Fortran extensions. If you want to try out the code(s) with other
compilers than those of the PGI suite, please note that CRAY currently does not
support nested OpenACC inside an OpenMP parallel region. This might, however,
be subject to change in the future.

Besides PGF90, an nvcc CUDA compiler must be available, as must a host and a
device math library. In the present codes, MKL and CUBLAS have been used.
If you want to use an alternative host math library (e.g., ATLAS), then
please change the paths in the Makefiles accordingly (i.e., MKL_INCL and MKL_LINK).

/ Janus Juul Eriksen

******

E-mail 1: jeriksen [at] uni-mainz [dot] de

E-mail 2: januseriksen [at] gmail [dot] com

