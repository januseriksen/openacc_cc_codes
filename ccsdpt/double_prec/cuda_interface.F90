module cuda_interface

  interface

     subroutine get_dev_mem ( total,free ) bind ( C , name="get_dev_mem" )

       use iso_c_binding

       implicit none

       integer (c_size_t) :: total, free

     end subroutine get_dev_mem

  end interface

end module cuda_interface
